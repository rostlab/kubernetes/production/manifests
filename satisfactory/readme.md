## Satisfactory

### status 
![image](https://argocd.rostlab.tech/api/badge?name=satisfactory&revision=true)

### purpose
Host a Satisfactory server for friends to play on.