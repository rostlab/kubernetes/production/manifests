## Unifi controller

### status 
![image](https://argocd.rostlab.tech/api/badge?name=unifi&revision=true)

### purpose
Deploy a UnifiOS application to manage all Unifi networking hardware.