# kubernetes

This repo hosts the entire homelab following the gitops principles.  

Use the steps below to install a cluster on an Ubuntu 22.04 server  

### Prereq
```commandline
sudo hwclock --hctosys 
sudo apt update && sudo apt upgrade -y
sudo apt-get install -y apt-transport-https ca-certificates curl gpg nfs-common ufw
```

### install containerd

Kubernetes is deprecating Docker as a container runtime after v1.20. By default, Kubernetes uses the Container Runtime Interface (CRI) to interface with your chosen container runtime. 
If you don't specify a runtime, kubeadm automatically tries to detect an installed container runtime by scanning through a list of known endpoints.
I've opted to go for containerd
```commandline
# Install containerd

wget https://github.com/containerd/containerd/releases/download/v1.7.11/containerd-1.7.11-linux-amd64.tar.gz
sudo tar Cxzvf /usr/local containerd-1.7.11-linux-amd64.tar.gz && rm containerd-1.7.11-linux-amd64.tar.gz

wget https://github.com/opencontainers/runc/releases/download/v1.1.11/runc.amd64
sudo install -m 755 runc.amd64 /usr/local/sbin/runc && rm runc.amd64

wget https://github.com/containernetworking/plugins/releases/download/v1.4.0/cni-plugins-linux-amd64-v1.4.0.tgz
sudo mkdir -p /opt/cni/bin
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.4.0.tgz && rm cni-plugins-linux-amd64-v1.4.0.tgz

sudo mkdir /etc/containerd
containerd config default | sudo tee /etc/containerd/config.toml

sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
sudo curl -L https://raw.githubusercontent.com/containerd/containerd/main/containerd.service -o /etc/systemd/system/containerd.service
sudo systemctl daemon-reload
sudo systemctl enable --now containerd
```

### solving ubuntu config issues
The default behavior of a kubelet was to fail to start if swap memory was detected on a node so we are disabling it. Network bridging is not setup by default so it is added.
```commandline
sudo swapoff -a  
sudo nano /etc/fstab # disable swap by commenting out the line with swap

echo "br_netfilter" | sudo tee -a /etc/modules
cat <<EOF | sudo tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
```


### install kubeadm
```commandline
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

### install helm
```commandline
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm
```

A reboot is nessesary to apply all configurations
```commandline
sudo reboot
```

### create cluster
```commandline
sudo kubeadm init
```

### kubeconfig
Export the kubeconfig to home dir to allow kubectl to consume it
```commandline
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

### Calico networking
```commandline
kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.27.0/manifests/calico.yaml
kubectl taint nodes --all node-role.kubernetes.io/control-plane-
```


### ArgoCD
install the core application, the UI and the cli
```commandline
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd && rm argocd-linux-amd64
```

Configuration of ArgoCD is done via a configmap
```commandline
kubectl apply -f - <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-cm
  namespace: argocd
  labels:
    app.kubernetes.io/name: argocd-cm
    app.kubernetes.io/part-of: argocd
data:
  statusbadge.enabled: "true"
  users.anonymous.enabled: "false"
  users.session.duration: "24h"
  admin.enabled: "true"
  accounts.luuk: apiKey, login
  exec.enabled: "true"
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: argocd-rbac-cm
  namespace: argocd
data:
  policy.default: role:readonly
  policy.csv: |
    p, role:org-admin, applications, *, */*, allow
    p, role:org-admin, clusters, get, *, allow
    p, role:org-admin, repositories, get, *, allow
    p, role:org-admin, repositories, create, *, allow
    p, role:org-admin, repositories, update, *, allow
    p, role:org-admin, repositories, delete, *, allow
    p, role:org-admin, projects, get, *, allow
    p, role:org-admin, projects, create, *, allow
    p, role:org-admin, projects, update, *, allow
    p, role:org-admin, projects, delete, *, allow
    p, role:org-admin, logs, get, *, allow
    p, role:org-admin, exec, create, */*, allow

    g, luuk, role:org-admin
EOF
```


In order for ArgoCD to sync repositories, it needs to be added to trusted repo's.

```commandline
kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: kubernetes
  namespace: argocd
  labels:
    argocd.argoproj.io/secret-type: repository
stringData:
  project: default
  url: https://gitlab.com/rostlab/kubernetes/manifests.git
EOF
```

Add application that monitors argocd-applications directory
```commandline
kubectl apply -f - <<EOF
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: argocd-applications
  namespace: argocd
spec:
  destination:
    name: ''
    namespace: argocd
    server: 'https://kubernetes.default.svc'
  source:
    path: argocd-applications
    repoURL: 'https://gitlab.com/rostlab/kubernetes/manifests.git'
    targetRevision: HEAD
  sources: []
  project: default
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
  revisionHistoryLimit: 0
operation:
  initiatedBy:
    username: admin
  sync:
    syncStrategy:
      hook: {}
EOF
```
# Letsencrypt
Get the Digital Ocean API token and Base64 encode it to be used as secret
```commandline
echo -ne {insert-digital-ocean-token} | base64
```

Setting the API token as a Kubernetes Secret to let Cert-Manager consume it. 
```commandline
kubectl apply -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: cert-manager
---
apiVersion: v1
kind: Secret
metadata:
  name: digital-ocean-api-token-secret
  namespace: cert-manager
data:
  # insert your API access token here
  api-token: "{insert-base64-Digital-Ocean-token}"
EOF
```


### Switch from Admin user to local user
```commandline
argocd login \
  --grpc-web argocd.rostlab.tech \
  --username admin \
  --password $(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
  
argocd account update-password \
  --account luuk \
  --current-password <current-user-password> \
  --new-password <new-user-password>
```

### Disable admin account:
```commandline
kubectl patch cm argocd-cm -n argocd --patch '{"data":{"admin.enabled": "false"}}'
```

## Kubeadm and MetalLB issue:
nodes have following label: ```node.kubernetes.io/exclude-from-external-load-balancers: ""```
causing MetalLB to ignore the node.


Fix this by removing the label:
```commandline
kubectl label node kubernetes node.kubernetes.io/exclude-from-external-load-balancers-
```