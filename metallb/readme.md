## Metal LB

### status 
![image](https://argocd.rostlab.tech/api/badge?name=metallb-system&revision=true)

### purpose
Loadbalance ingress controllers over multiple nodes by using L2 advertisement.

### Known issues

Kubeadm clusters seems to have this label (node.kubernetes.io/exclude-from-external-load-balancers) by default for control plane nodes (notably, in kind its typical to run a single node). Removing this label will include the node in L2 advertisement.

