## Digital Ocean DNS updater

### status 
![image](https://argocd.rostlab.tech/api/badge?name=do-dns-updater&revision=true)

### purpose
Update public DNS records to point to dynamic WAN ip